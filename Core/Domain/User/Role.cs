﻿using System;
using Core.Contracts;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Core.Domain.User
{
    public class Role : IdentityRole<int, UserRole>,ITimeStamp, IEntity
    {
        public new int Id { get; set; }

        public Role()
        {
            CreatedUtc = DateTime.UtcNow;
        }

        public Role(string name)
        {
            Name = name;
        }

        public DateTime CreatedUtc { get; set; }
    }
}