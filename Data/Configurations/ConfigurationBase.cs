﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Contracts;

namespace Data.Configurations
{
    internal abstract class ConfigurationBase<T> : EntityTypeConfiguration<T> where T : class, IEntity
    {
        protected ConfigurationBase()
        {
            HasKey(x => x.Id);
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
    

